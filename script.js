(function(){
    "use strict";

    /* Polyfill - Closest
    *******************************************************/
    if(!Element.prototype.matches) {
        Element.prototype.matches = Element.prototype.msMatchesSelector || 
                                Element.prototype.webkitMatchesSelector;
    }
    
    if(!Element.prototype.closest) {
        Element.prototype.closest = function(s) {
            var el = this;
            do {
                if (Element.prototype.matches.call(el, s)) return el;
                el = el.parentElement || el.parentNode;
            } while (el !== null && el.nodeType === 1);

            return null;
        };
    }

    
    /* Polyfill - Current Script
    *******************************************************/
    var currentScript = 'currentScript';

    // If browser needs currentScript polyfill, add get currentScript() to the document object
    if (!(currentScript in document)) {
        Object.defineProperty(document, currentScript, {
        get: function () {
            // IE 8-10 support script readyState
            // IE 11+ support stack trace
            try {
                throw new Error();
            }
            catch (err) {
            // Find the second match for the "at" string to get file src url from stack.
            // Specifically works with the format of stack traces in IE.
            var i = 0,
                stackDetails = (/.*at [^(]*\((.*):(.+):(.+)\)$/ig).exec(err.stack),
                scriptLocation = (stackDetails && stackDetails[1]) || false,
                line = (stackDetails && stackDetails[2]) || false,
                currentLocation = document.location.href.replace(document.location.hash, ''),
                pageSource,
                inlineScriptSourceRegExp,
                inlineScriptSource,
                scripts = document.getElementsByTagName('script'); // Live NodeList collection

            if (scriptLocation === currentLocation) {
                pageSource = document.documentElement.outerHTML;
                inlineScriptSourceRegExp = new RegExp('(?:[^\\n]+?\\n){0,' + (line - 2) + '}[^<]*<script>([\\d\\D]*?)<\\/script>[\\d\\D]*', 'i');
                inlineScriptSource = pageSource.replace(inlineScriptSourceRegExp, '$1').trim();
            }

            for (; i < scripts.length; i++) {
                // If ready state is interactive, return the script tag
                if (scripts[i].readyState === 'interactive') {
                return scripts[i];
                }

                // If src matches, return the script tag
                if (scripts[i].src === scriptLocation) {
                return scripts[i];
                }

                // If inline source matches, return the script tag
                if (
                scriptLocation === currentLocation &&
                scripts[i].innerHTML &&
                scripts[i].innerHTML.trim() === inlineScriptSource
                ) {
                return scripts[i];
                }
            }

            // If no match, return null
            return null;
            }
        }
        });
    }


    var Ajax, Utils, Template, Data, App;

    
    // get Current Path
    var script  = document.currentScript,
        fullUrl = script.src,
        dirUrl  = fullUrl.substring(0, fullUrl.lastIndexOf('/'));


    /* Ajax Function
    *******************************************************/
    Ajax = (function(){
        function get(url, successCallback, errorCallback){
            var xhr = new XMLHttpRequest();

            xhr.onload = function(){
                if(xhr.status === 200){
                    if(typeof successCallback === "function")
                        successCallback(xhr.response);
                }else{
                    if(typeof errorCallback === "function")
                        errorCallback(xhr.response);
                }
            }
            xhr.open('GET', url);
            xhr.send();
        }

        function post(url, data, successCallback, errorCallback){

        }

        function getScript(url, successCallback){
            // var dirUrl = location.href.substring(0, location.href.lastIndexOf('/'));

            var script  = document.createElement('script'),
                head    = document.getElementsByTagName('head')[0];

            script.async    = 1;
            script.type     = 'text/javascript';
            script.src      = url;
            script.onload   = function(response){
                if(typeof successCallback === "function")
                    successCallback(response);
            }

            head.appendChild(script);
        }

        function multiple(requests, callback){
            if(!Array.isArray(requests) || !requests.length)
                return;

            var count = 0, request = {}, responses = [];

            for(var i=0;i<requests.length;i++){
                request = requests[i];

                switch(request.type.toLowerCase()){
                    case('post'):
                        post(
                            request.url,
                            request.data,
                            onDone.bind({result:'success', request: request}),
                            onDone.bind({result:'error', request: request})
                        );
                        break;
                    case('get'):
                        get(
                            request.url,
                            onDone.bind({result:'success', request: request}),
                            onDone.bind({result:'error', request: request})
                        );
                        break;
                    case('getscript'):
                        getScript(
                            request.url,
                            onDone.bind({result:'success', request: request})
                        );
                        break;
                }
            }

            function onDone(response){
                count += 1;
                responses.push(response);

                if(this.result === 'success'){
                    if(this.request.hasOwnProperty('onSuccess'))
                        this.request.onSuccess(response);
                }

                if(this.result === 'error'){
                    if(this.request.hasOwnProperty('onError'))
                        this.request.onError(response);
                }

                if(count === requests.length){
                    if(typeof callback === "function")
                        callback(responses);
                }
            }
        }

        return {
            get: get,
            post: post,
            getScript: getScript,
            multiple: multiple
        }
    })();


    /* Utilities
    *******************************************************/
    Utils = (function(){
        function _prepareElement(element){
            if(!element) return;

            var elements = [];

            if(element.tagName){
                elements = [element];
            }

            if(typeof(element) === 'string'){
                elements = document.querySelectorAll(element);
            }

            return elements;
        }

        function classList(type, element, className){
            var elements = _prepareElement(element);

            for(var i=0;i<elements.length;i++){
                switch(type){
                    case('add'):
                        elements[i].classList.add(className);
                        break;
                    case('remove'):
                        elements[i].classList.remove(className);
                        break;
                    case('has'):
                        return elements[i].classList.contains(className);
                }
            }
        }

        function onEvent(){
            var element = arguments[0],
                event   = arguments[1];

            if(typeof arguments[2] === 'string'){
                var target      = arguments[2],
                    callback    = arguments[3];

                window.addEventListener(event, function(e){
                    var path    = e.path || (e.composedPath && e.composedPath()) || Utils.eventPath(e),
                        targets = element.querySelectorAll(target);

                    for(var i=0;i<targets.length;i++){
                        if(path.indexOf(targets[i]) >= 0){
                            callback.call(targets[i], e);
                        }
                    }
                });
            }

            if(typeof arguments[2] === 'function'){
                element.addEventListener(event, arguments[2]);
            }
        }

        function eventPath(evt) {
            var path = (evt.composedPath && evt.composedPath()) || evt.path,
                target = evt.target;

            if (path != null) {
                // Safari doesn't include Window, and it should.
                path = (path.indexOf(window) < 0) ? path.concat([window]) : path;
                return path;
            }

            if (target === window) {
                return [window];
            }

            function getParents(node, memo) {
                memo = memo || [];
                var parentNode = node.parentNode;

                if (!parentNode) {
                    return memo;
                }
                else {
                    return getParents(parentNode, memo.concat([parentNode]));
                }
            }

            return [target]
                .concat(getParents(target))
                .concat([window]);
        }

          
        return {
            classList: classList,
            onEvent: onEvent,
            eventPath: eventPath
        }
    })();


    /* Data Wrapper
    *******************************************************/
    Data = (function(){
        var _data = {};

        function set(data){
            _data = data;
        }

        function getAll(){
            return _data;
        }

        function getById(id){
            if(_data.hasOwnProperty(id))
                return _data[id];
            else
                return '';
        }

        return {
            set: set,
            getAll: getAll,
            getById: getById
        }
    })();


    /* Templates
    *******************************************************/
    Template = (function(){
        var _templates = {
            main: '<div class="tlcgi">'+
                    '<h1 class="tlcgi-title">%%TITLE%%</h1>'+
                    '<p class="tlcgi-desc">%%DESC%%</p>'+
                    '<div class="tlcgi-global-map"><div class="tlcgi-map">%%GLOBAL_MAP%% %%MAP_PALETTE%% <span class="tlcgi-name-tag">Global Map</span></div> %%CHILD_MAPS%%</div>'+
                    '<div class="tlcgi-global-content tlcgi-content">%%CONTENT%%</div>'+
                    '</div>',
            childMap: '<div class="tlcgi-child-map" id="child-map-%%ID%%">'+
                        '<div class="tlcgi-map">%%MAP%%</div>'+
                        '</div>',
            mapPalette: '<div class="tlcgi-map-palette tlcgi-content %%CLASS%%">%%LIST%%<button class="tlcgi-close" type="button"></button></div>',
            mapModal: '<div class="tlcgi-map-modal" id="map-modal-%%ID%%">'+
                        '<button class="tlcgi-map-modal-close" type="button">x close</button>'+
                        '<div class="tlcgi-map-modal-wrap">'+
                        '<div class="tlcgi-map-modal-main">'+
                        '<h2 class="tlcgi-map-modal-title">%%TITLE%%</h2>'+
                        '<p class="tlcgi-map-modal-desc">%%DESC%%</p>'+
                        '<div class="tlcgi-map"><div class="tlcgi-map-inner">%%MAP%% %%MAP_PINS%%</div> %%MAP_PALETTE%% %%PIN_MODALS%% <span class="tlcgi-name-tag">Global Map</span></div>'+
                        '</div>'+
                        '<div class="tlcgi-map-modal-side">'+
                        '<div class="tlcgi-map-modal-side-content tlcgi-content"><article>%%CONTENT%%</article></div>'+
                        '</div>'+
                        '</div>'+
                        '</div>',
            mapPins: '<div class="tlcgi-map-pins">%%PINS%%</div>',
            pinModals: '<div class="tlcgi-pin-modals">%%PIN_MODALS%%</div>',
            pinModal: '<div class="tlcgi-pin-modal" id="pin-modal-%%CODE%%">'+
                        '<div class="tlcgi-pin-modal-head"><h3>%%TITLE%%</h3>'+
                        '<button class="tlcgi-pin-modal-close" type="button" data-code="%%CODE%%">x</button></div>'+
                        '<div class="tlcgi-pin-modal-body"><div class="tlcgi-content">%%CONTENT%%</div></div>'+
                        '</div>',
            columns: '<div class="tlcgi-columns columns-%%COUNT%%">%%COLUMNS%%</div>',
            column: '<div class="tlcgi-column">%%CONTENT%%</div>'
        };

        function get(id, variables){
            if(!_templates.hasOwnProperty(id))
                return;

            var template = _templates[id];

            if(typeof variables !== 'object')
                return template;

            var keys    = Object.keys(variables).map(function(i){ return '%%'+i+'%%' }).join('|'),
                pattern = new RegExp(keys, 'g');
            
            return template.replace(pattern, function(matched){
                return variables[matched.replace(/%/g, '')];
            });
        }

        function splitContent(content){
            var contents = content.split('<!--SPLIT-->');
            
            if(!contents.length) return content;

            var output = '';

            for(var i=0;i<contents.length;i++){
                output += get('column', {
                    CONTENT: contents[i]
                });
            }

            return get('columns', {
                COUNT: contents.length,
                COLUMNS: output
            });
        }

        return {
            get: get,
            splitContent: splitContent
        }
    })();


    /* Application
    *******************************************************/
    App = (function(){
        var _container;

        function _showNameTag(e){
            var map     = this.closest('.tlcgi-map'),
                tag     = map.querySelector('.tlcgi-name-tag'),
                name    = this.getAttribute('data-name') || this.parentNode.getAttribute('data-name');

            var mapOffset = map.getBoundingClientRect();
                
            tag.style.display   = 'block';
            tag.style.left      = Math.abs(mapOffset.left-e.clientX-10)+'px';
            tag.style.top       = Math.abs(mapOffset.top-e.clientY-10)+'px';
            tag.innerText       = name;
        }

        function _hideNameTag(){
            var tag = this.closest('.tlcgi-map').querySelector('.tlcgi-name-tag');
            
            tag.innerText       = '';
            tag.style.display   = 'none';
        }

        function _getMapPalette(closed){
            var list    = Data.getById('mapPalette'),
                output  = '<ul>';

            for(var i=0;i<list.length;i++){
                output += '<li data-color="'+list[i].color+'"><span style="background-color:'+list[i].color+'"></span><span>'+list[i].label+'</span></li>';
            }

            output += '</ul>';

            return Template.get('mapPalette', { 
                LIST: output,
                CLASS: closed ?'is-closed' :''
            });
        }

        function _closeMapPalette(){
            var mapPalette  = this.parentNode,
                palette     = mapPalette.querySelectorAll('ul');

            if(Utils.classList('has', mapPalette, 'is-closed')){
                TweenLite.to(palette, 0.3, {
                    display: 'block',
                    autoAlpha: 1,
                    onComplete: function(){
                        Utils.classList('remove', mapPalette, 'is-closed');
                    }
                });
            }else{
                TweenLite.to(palette, 0.3, {
                    display: 'none',
                    autoAlpha: 0,
                    onComplete: function(){
                        Utils.classList('add', mapPalette, 'is-closed');
                    }
                });
                TweenLite.set(palette, {display: 'block', autoAlpha: 1, delay: 0.3});
            }

        }

        function _fixMapOnModal(){
            var scrolled = this.scrollTop;
            this.querySelector('.tlcgi-map').style.marginTop = scrolled+'px';
        }

        function _setMapPins(countries){
            var pins = '';

            for(var i=0;i<countries.length;i++){
                var c = countries[i];

                pins += '<span class="tlcgi-pin" id="pin-'+c.code+'" data-code="'+c.code+'" data-name="'+c.name+'" data-alt-name="'+(c.hasOwnProperty('alt_name') ?c.alt_name :c.name)+'" style="top:'+c.position.top+'%;left:'+c.position.left+'%;"></span>';
            }

            return Template.get('mapPins', {
                PINS: pins
            });
        }

        function _showMapPinModal(){
            var code    = this.getAttribute('data-code'),
                modal   = document.querySelector('.tlcgi-pin-modal#pin-modal-'+code);
                
            if(!modal) return;

            Utils.classList('add', modal, 'active');
            Utils.classList('add', modal.parentNode, 'show');
        }

        function _hideMapPinModal(){
            var code    = this.getAttribute('data-code'),
                modal   = document.querySelector('.tlcgi-pin-modal#pin-modal-'+code);

            Utils.classList('remove', modal, 'active');
            Utils.classList('remove', modal.parentNode, 'show');
        }

        function _setPinModals(countries){
            var modals = '';

            for(var c=0;c<countries.length;c++){
                var country = countries[c];

                var content = '';

                for(var i=0;i<country.content.length;i++){
                    var title = '';

                    switch(i){
                        case(0): title = 'Existing TLCB (period)';break;
                        case(1): title = 'New COVID-19 TLCB (period)';break;
                        case(2): title = 'Remarks';break;
                    }

                    if(country.content[i] !== '')
                        content += '<h4>'+title+'</h4>'+country.content[i];
                }
                
                modals += Template.get('pinModal', {
                    TITLE: country.name,
                    CODE: country.code,
                    CONTENT: content
                });
            }

            return Template.get('pinModals', {
                PIN_MODALS: modals
            });
        }

        function _childMapsTransition(){
            var fromMap     = this.from,
                toMap       = this.to,
                clone       = fromMap.cloneNode(true),
                before      = this.before,
                after       = this.after;
                
            var from    = _calculatePosition(fromMap),
                to      = _calculatePosition(toMap);


            if(typeof before === 'function')
                before();
            
            TweenLite.set([fromMap, toMap], { visibility: "hidden", scrollTop: 0 });
            TweenLite.set(clone, { position: "absolute", margin: 0, opacity: 1, visibility: 'visible', backgroundColor: '#fff' });
            
            document.body.appendChild(clone);

            Utils.classList('add', document.body, 'tlcgi-transitioning');
                
            var style = {
                x: to.left - from.left,
                y: to.top - from.top,
                width: to.width,
                height: to.height,
                autoRound: false,
                ease: Power1.easeOut,
                onComplete: function(){
                    TweenLite.set(toMap, { visibility: "visible" });
                    document.body.removeChild(clone);
                    Utils.classList('remove', document.body, 'tlcgi-transitioning');

                    if(typeof after === 'function')
                        after();
                }
            };
            
            TweenLite.set(clone, from);  
            TweenLite.to(clone, 0.8, style);
        }


        function _calculatePosition(element){
            var root    = document.documentElement,
                body    = document.body;
                
            var rect = element.getBoundingClientRect();
            
            var scrollTop  = window.pageYOffset || root.scrollTop  || body.scrollTop  || 0;
            var scrollLeft = window.pageXOffset || root.scrollLeft || body.scrollLeft || 0;
            
            var clientTop  = root.clientTop  || body.clientTop  || 0;
            var clientLeft = root.clientLeft || body.clientLeft || 0;
            
            return {
                top: Math.round(rect.top + scrollTop - clientTop),
                left: Math.round(rect.left + scrollLeft - clientLeft),
                height: rect.height,
                width: rect.width,
            };
        }

        function _fixSVGonIE(){
            if(window.document.documentMode){
                var svg = document.querySelectorAll('svg[data-ie-ratio]');

                for(var i=0;i<svg.length;i++){
                    var ratio = JSON.parse(svg[i].getAttribute('data-ie-ratio'));

                    svg[i].setAttribute('preserveAspectRatio', 'xMidYMin slice');
                    svg[i].style.width = '100%';
                    svg[i].style.overflow = 'visible';
                    svg[i].style.height = '1px';
                    svg[i].style.paddingBottom = (ratio[1]/ratio[0]*100)+'%';
                }
            }
        }

        function _eventHandlers(){
            Utils.onEvent(document, 'click', '.tlcgi-map-palette .tlcgi-close', _closeMapPalette);

            var pathTriggers = [
                {
                    paths: ['Europe'],
                    childMap: '#child-map-europe',
                    mapModal: '#map-modal-europe'
                },
                {
                    paths: ['South America', 'North America'],
                    childMap: '#child-map-americas',
                    mapModal: '#map-modal-americas'
                },
                {
                    paths: ['Africa'],
                    childMap: '#child-map-africa',
                    mapModal: '#map-modal-africa'
                },
                {
                    paths: ['Asia', 'Australia & Oceania'],
                    childMap: '#child-map-asia',
                    mapModal: '#map-modal-asia'
                }
            ];
            pathTriggers.map(function(trigger){
                trigger.paths.map(function(path){
                    var childMap    = document.querySelector(trigger.childMap),
                        mapModal    = document.querySelector(trigger.mapModal);

                    Utils.onEvent(_container, 'click', '.tlcgi-global-map .tlcgi-map svg [data-name="'+path+'"] path', _childMapsTransition.bind({
                        from: childMap,
                        to: mapModal,
                        before: function(){
                        },
                        after: function(){
                            Utils.classList('add', document.body, 'tlcgi-child-map-open');
                            
                            setTimeout(function(){
                                Utils.classList('add', mapModal, 'tlcgi-ready');
                            }, 300);
                        }
                    }));

                    Utils.onEvent(mapModal, 'click', '.tlcgi-map-modal-close', _childMapsTransition.bind({
                        from: mapModal,
                        to: childMap,
                        before: function(){
                        },
                        after: function(){
                            Utils.classList('remove', document.body, 'tlcgi-child-map-open');
                            Utils.classList('remove', mapModal, 'tlcgi-ready');
                        }
                    }));
                });
            });
            
            var svgs = document.querySelectorAll('.tlcgi-map svg path');
            for(var i=0;i<svgs.length;i++){
                svgs[i].addEventListener('click', function(){});
            }

            
            var pins = document.querySelectorAll('.tlcgi-pin');
            for(var i=0;i<pins.length;i++){
                pins[i].addEventListener('click', function(){});
            }
            Utils.onEvent(document, 'click', '.tlcgi-pin', _showMapPinModal);
            Utils.onEvent(document, 'click', '.tlcgi-pin-modal-close', _hideMapPinModal);


            var nameTags = document.querySelectorAll('.tlcgi-map');
            for(var i=0;i<nameTags.length;i++){
                Utils.onEvent(nameTags[i], 'mousemove', 'svg [data-name=Countries] path', _showNameTag);
                Utils.onEvent(nameTags[i], 'mouseout', 'svg [data-name=Countries] path', _hideNameTag);
            }

            var nameTags = document.querySelectorAll('.tlcgi-map-modal');
            for(var i=0;i<nameTags.length;i++){
                Utils.onEvent(nameTags[i], 'click', '.tlcgi-map svg [data-name=Countries] path', function(){
                    var name = this.getAttribute('data-name') || this.parentNode.getAttribute('data-name');

                    var elem = document.querySelector('.tlcgi-pin[data-name="'+name+'"]');

                    if(!elem){
                        elem = document.querySelector('.tlcgi-pin[data-alt-name="'+name+'"]');
                        console.log('used alternative name.');
                    }
                    
                    elem.click();
                });
            }

        }

        function _render(){
            var globalMap, childMaps, mapModals;

            // prepare requests for continents images
            var childMapRequests = Data.getById('continents').map(function(map){
                return {
                    type: 'get',
                    url: dirUrl+'/img/'+map.mapFile,
                    onSuccess: function(response){
                        if(!childMaps) childMaps = '';
                        childMaps += Template.get('childMap', {
                            ID: map.id,
                            MAP: response,
                        });

                        if(!mapModals) mapModals = '';
                        mapModals += Template.get('mapModal', {
                            ID: map.id,
                            MAP: response,
                            TITLE: map.name,
                            DESC: 'Click a red pin to read jurisdiction-specific TLCB developments',
                            CONTENT: Template.splitContent(map.content),
                            MAP_PALETTE: _getMapPalette(),
                            MAP_PINS: _setMapPins(map.countries),
                            PIN_MODALS: _setPinModals(map.countries)
                        });
                    }
                }
            });

            Ajax.multiple([
                {
                    type: 'get',
                    url: dirUrl+'/img/map-global.svg',
                    onSuccess: function(response){ globalMap = response }
                }
            ].concat(childMapRequests), function(){
                _container.innerHTML = Template.get('main', {
                    TITLE: 'Tax Loss Carryback Global Insights',
                    DESC: 'Click on a region to enlarge and read TLCB developments',
                    GLOBAL_MAP: globalMap,
                    MAP_PALETTE: _getMapPalette(),
                    CHILD_MAPS: childMaps,
                    CONTENT: Template.splitContent(Data.getById('mainContent'))
                });

                document.body.querySelector('.tlcgi-map-modals').innerHTML = mapModals;
                
                _eventHandlers();
                _fixSVGonIE();

                Utils.classList('add', _container, 'tlcgi-loaded');
            });
        }

        function _setMapModalContainer(){
            var container = document.createElement('div');
            
            container.classList.add('tlcgi-map-modals');
            document.body.appendChild(container);
        }

        function init(){
            Ajax.multiple([
                {
                    type: 'get',
                    url: dirUrl+'/data.json',
                    onSuccess: function(response){
                        Data.set(JSON.parse(response));
                    }
                },
                {
                    type: 'getScript',
                    url: dirUrl+'/vendor/gsap.min.js',
                    onSuccess: function(response){
                    }
                },
                {
                    type: 'getScript',
                    url: dirUrl+'/vendor/interact.min.js',
                    onSuccess: function(response){
                        // target elements with the "draggable" class
                        interact('.tlcgi-map-palette').draggable({
                            // enable inertial throwing
                            inertia: true,
                            // keep the element within the area of it's parent
                            modifiers: [
                                interact.modifiers.restrictRect({
                                restriction: 'parent',
                                endOnly: true
                                })
                            ],
                            // enable autoScroll
                            autoScroll: true,

                            listeners: {
                                // call this function on every dragmove event
                                move: dragMoveListener,

                                // call this function on every dragend event
                                end: function(event){
                                    var textEl = event.target.querySelector('p');

                                    textEl && (textEl.textContent =
                                        'moved a distance of ' +
                                        (Math.sqrt(Math.pow(event.pageX - event.x0, 2) +
                                                Math.pow(event.pageY - event.y0, 2) | 0))
                                        .toFixed(2) + 'px')
                                }
                            }
                        });

                        function dragMoveListener(event){
                            var target = event.target
                            // keep the dragged position in the data-x/data-y attributes
                            var x = (parseFloat(target.getAttribute('data-x')) || 0) + event.dx
                            var y = (parseFloat(target.getAttribute('data-y')) || 0) + event.dy

                            // translate the element
                            target.style.webkitTransform =
                            target.style.transform =
                                'translate(' + x + 'px, ' + y + 'px)'

                            // update the posiion attributes
                            target.setAttribute('data-x', x)
                            target.setAttribute('data-y', y)
                        }

                        // this function is used later in the resizing and gesture demos
                        window.dragMoveListener = dragMoveListener;
                    }
                }
            ], function(){
                _container = document.querySelector('#tlcgi-container');

                _setMapModalContainer();
                _render();
    
                setTimeout(function(){
                    Utils.classList('add', 'body', 'tlcgi-initialized');
                    console.log('tlcgi initialized.');
                }, 500);
            });
        }

        return {
            init: init
        }
    })();


    window.addEventListener('DOMContentLoaded', function(){
        App.init();
    });

})();